'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var HashHelper = function () {
    function HashHelper() {
        _classCallCheck(this, HashHelper);
    }

    _createClass(HashHelper, null, [{
        key: 'generateHash',
        value: function generateHash(plainTextPassword) {
            _bcrypt2.default.hash(plainTextPassword, 10, function (err, hash) {
                if (err) {
                    console.log('if part');
                    console.log(err);
                } else {
                    return hash;
                }
            });
        }
    }]);

    return HashHelper;
}();

exports.default = HashHelper;