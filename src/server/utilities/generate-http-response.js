'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var HttpReponseUtil = function () {
    function HttpReponseUtil() {
        _classCallCheck(this, HttpReponseUtil);
    }

    _createClass(HttpReponseUtil, null, [{
        key: 'ok',
        value: function ok(res, obj) {
            res.statusCode = 200;
            res.send(JSON.stringify(obj, null, 4));
        }
    }, {
        key: 'created',
        value: function created(res, obj) {
            res.statusCode = 201;
            res.send(JSON.stringify(obj, null, 4));
        }
    }, {
        key: 'notFound',
        value: function notFound(res, obj) {
            res.statusCode = 404;
            res.send(JSON.stringify({ error: 'Resource not found', details: obj }, null, 4));
        }
    }, {
        key: 'unauthorized',
        value: function unauthorized(res, obj) {
            res.statusCode = 401;
            res.send(JSON.stringify({ error: 'Unauthorized opperation', details: obj }, null, 4));
        }
    }, {
        key: 'badRequest',
        value: function badRequest(res, obj) {
            res.statusCode = 400;
            res.send(JSON.stringify({ error: 'Bad Request', details: obj }, null, 4));
        }
    }, {
        key: 'internalServerError',
        value: function internalServerError(res, obj) {
            res.statusCode = 500;
            res.send(JSON.stringify({ error: 'Internal Server Error', details: obj }, null, 4));
        }
    }]);

    return HttpReponseUtil;
}();

exports.default = HttpReponseUtil;