"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _generateHttpResponse = require('../utilities/generate-http-response');

var _generateHttpResponse2 = _interopRequireDefault(_generateHttpResponse);

var _agent = require('../business/agent');

var _agent2 = _interopRequireDefault(_agent);

var _agentDetails = require('./specification/agent-details');

var _agentDetails2 = _interopRequireDefault(_agentDetails);

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var agentManagement = _express2.default.Router();
var agent = new _agent2.default();

function validateUplaodAgentDataModel(req, res, next) {
    new _agentDetails2.default().validate(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.badRequest(res, err);
            return false;
        } else {
            next();
            return true;
        }
    });
}

//get all agents from database
agentManagement.get('', function (req, res) {
    agent.list('', function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else {
            _generateHttpResponse2.default.ok(res, value);
        }
    });
});

//update info of agents
agentManagement.put('/:email', function (req, res) {
    if (_validator2.default.isEmail(req.params.email)) {
        agent.update(req.params.email, req.body, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'uuid is invalid or user not registered');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Email ID Invalid');
    }
});

//add agent to database
agentManagement.post('', validateUplaodAgentDataModel, function (req, res) {
    agent.add(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else if (value) {
            _generateHttpResponse2.default.created(res, value);
        } else {
            _generateHttpResponse2.default.badRequest(res, 'Agent already exist');
        }
    });
});

//remove agent from database
agentManagement.delete('/:uuid', function (req, res) {
    if (req.params.uuid) {
        agent.remove(req.params.uuid, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'EmailId not found');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid email id');
    }
});

exports.default = agentManagement;