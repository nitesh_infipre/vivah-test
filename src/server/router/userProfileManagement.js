"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _generateHttpResponse = require('../utilities/generate-http-response');

var _generateHttpResponse2 = _interopRequireDefault(_generateHttpResponse);

var _userProfile = require('../business/userProfile');

var _userProfile2 = _interopRequireDefault(_userProfile);

var _userProfileDetails = require('./specification/userProfile-details');

var _userProfileDetails2 = _interopRequireDefault(_userProfileDetails);

var _workInfoManagement = require('./workInfoManagement');

var _workInfoManagement2 = _interopRequireDefault(_workInfoManagement);

var _educationInfoManagement = require('./educationInfoManagement');

var _educationInfoManagement2 = _interopRequireDefault(_educationInfoManagement);

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var userProfileManagement = _express2.default.Router();
var profile = new _userProfile2.default();

userProfileManagement.use('/workInfo', _workInfoManagement2.default);
userProfileManagement.use('/educationInfo', _educationInfoManagement2.default);

function validateUserProfileDataModel(req, res, next) {
    new _userProfileDetails2.default().validate(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.badRequest(res, err);
            return false;
        } else {
            next();
            return true;
        }
    });
}

//add profile info to database
userProfileManagement.post('', validateUserProfileDataModel, function (req, res) {
    profile.add(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else if (value) {
            _generateHttpResponse2.default.created(res, value);
        } else {
            _generateHttpResponse2.default.badRequest(res, err);
        }
    });
});

//update profile
userProfileManagement.put('/:uuid', function (req, res) {
    if (req.params.uuid) {
        profile.update(req.params.uuid, req.body, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'Invalid UUID');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid UUID');
    }
});

//get all profile info from database
userProfileManagement.get('', function (req, res) {
    profile.list('', function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else {
            _generateHttpResponse2.default.ok(res, value);
        }
    });
});

userProfileManagement.get('/:uuid', function (req, res) {

    profile.list(req.params.uuid, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else if (value && value.length > 0) {
            _generateHttpResponse2.default.ok(res, value);
        } else {
            _generateHttpResponse2.default.notFound(res, 'EmailId not found');
        }
    });
});

//delete user profile
userProfileManagement.delete('/:uuid', function (req, res) {
    if (req.params.uuid) {
        profile.remove(req.params.uuid, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'UUID Not found');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid uuid');
    }
});

var storage = _multer2.default.diskStorage({
    destination: function destination(req, file, callback) {
        callback(null, './uploads');
    },
    filename: function filename(req, file, callback) {
        callback(null, file.fieldname + '-' + Date.now() + _path2.default.extname(file.originalname));
    }
});

/* 
Check if add personal information is added to databse
*/
userProfileManagement.get('/userAgent/:uuid', function (req, res) {
    profile.getUserStatus(req.params.uuid, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else if (value && value.length > 0) {
            _generateHttpResponse2.default.ok(res, value);
        } else {
            _generateHttpResponse2.default.notFound(res, 'EmailId not found');
        }
    });
});

/* 
Get personal_info uuid based on user uuid
*/
userProfileManagement.get('/userPersonalInfoId/:uuid', function (req, res) {
    profile.getUserPersonalInfoId(req.params.uuid, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else if (value && value.length > 0) {
            _generateHttpResponse2.default.ok(res, value);
        } else {
            _generateHttpResponse2.default.notFound(res, 'EmailId not found');
        }
    });
});

/* 
Upload Profile pic and save it in database
*/
userProfileManagement.post('/avatar/:uuid', function (req, res) {

    var upload = (0, _multer2.default)({
        storage: storage,
        fileFilter: function fileFilter(req, file, callback) {
            var ext = _path2.default.extname(file.originalname);
            if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
                return callback(new Error('Only images are allowed'));
            }
            callback(null, true);
        },
        limits: {
            fileSize: 1024 * 1024
        }
    }).single('avatar');

    upload(req, res, function (err) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err.message);
            return;
        }

        profile.uploadProfile(req.params.uuid, req.file.filename, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'UUID Not found');
            }
        });
    });
});

exports.default = userProfileManagement;