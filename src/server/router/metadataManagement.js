"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _generateHttpResponse = require('../utilities/generate-http-response');

var _generateHttpResponse2 = _interopRequireDefault(_generateHttpResponse);

var _metadata = require('../business/metadata');

var _metadata2 = _interopRequireDefault(_metadata);

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var metadataManagement = _express2.default.Router();
var meta = new _metadata2.default();
//get states information
metadataManagement.get('/states', function (req, res) {
	meta.states(function (err, value) {
		if (err) {
			_generateHttpResponse2.default.internalServerError(res, err);
		} else {
			_generateHttpResponse2.default.ok(res, value);
		}
	});
});

//get city information based on state id
metadataManagement.get('/city/:stateId', function (req, res) {
	if (!_validator2.default.isInt(req.params.stateId)) {
		_generateHttpResponse2.default.badRequest(res, 'provide valid state id');
	}

	meta.city(req.params.stateId, function (err, value) {
		if (err) {
			_generateHttpResponse2.default.internalServerError(res, err);
		} else {
			_generateHttpResponse2.default.ok(res, value);
		}
	});
});

//get religions
metadataManagement.get('/religion', function (req, res) {
	meta.religion(function (err, value) {
		if (err) {
			_generateHttpResponse2.default.internalServerError(res, err);
		} else {
			_generateHttpResponse2.default.ok(res, value);
		}
	});
});

//get cultures
metadataManagement.get('/culture', function (req, res) {
	meta.culture(function (err, value) {
		if (err) {
			_generateHttpResponse2.default.internalServerError(res, err);
		} else {
			_generateHttpResponse2.default.ok(res, value);
		}
	});
});

//marital status 
metadataManagement.get('/maritalStatus', function (req, res) {
	meta.maritalStatus(function (err, value) {
		if (err) {
			_generateHttpResponse2.default.internalServerError(res, err);
		} else {
			_generateHttpResponse2.default.ok(res, value);
		}
	});
});

//get work type
metadataManagement.get('/worktype', function (req, res) {
	meta.worktype(function (err, value) {
		if (err) {
			_generateHttpResponse2.default.internalServerError(res, err);
		} else {
			_generateHttpResponse2.default.ok(res, value);
		}
	});
});

metadataManagement.get('/all', function (req, res) {
	meta.all(function (err, value) {
		if (err) {
			_generateHttpResponse2.default.internalServerError(res, err);
		} else {
			_generateHttpResponse2.default.ok(res, value);
		}
	});
});

exports.default = metadataManagement;