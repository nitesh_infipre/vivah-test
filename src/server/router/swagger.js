'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var options = {
    swaggerDefinition: {
        info: {
            title: 'API Documentation',
            version: '1.0.0'
        },
        host: 'localhost:3000',
        basePath: '/api'
    },
    apis: ['./build/server/router/*/*.js', './build/server/router/*.js']
};

exports.default = options;