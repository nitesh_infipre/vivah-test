"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _generateHttpResponse = require('../utilities/generate-http-response');

var _generateHttpResponse2 = _interopRequireDefault(_generateHttpResponse);

var _search = require('../business/search');

var _search2 = _interopRequireDefault(_search);

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var searchManagement = _express2.default.Router();
var ser = new _search2.default();

//get all users from database
/* searchManagement.get('', (req, res) => {
    ser.list('', (err, value) => {
        if (err) {
            HttpReponseUtil.internalServerError(res, err);
        } else {
            HttpReponseUtil.ok(res, value);
        }
    });
}); */

//get user based on email-id
searchManagement.get('/:email', function (req, res) {
    if (_validator2.default.isEmail(req.params.email)) {
        req.params.email = _validator2.default.normalizeEmail(req.params.email);
        ser.list(req.params.email, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.length > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'EmailId not found');
            }
        });
    } else {
        res.send('in valid email');
    }
});

searchManagement.get('/name/:name', function (req, res) {

    if (req.params.name) {
        ser.listName(req.params.name, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.length > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'Name not found');
            }
        });
    } else {
        res.send('in valid data');
    }
});

searchManagement.get('/Name/:name/:email', function (req, res) {
    // res.send('Name');
    if (req.params.name && req.params.email) {
        ser.listDetails(req.params.name, req.params.email, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.length > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'Name  & email not found');
            }
        });
    } else {
        res.send('in valid data');
    }
});
exports.default = searchManagement;