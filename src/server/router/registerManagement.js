"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _generateHttpResponse = require('../utilities/generate-http-response');

var _generateHttpResponse2 = _interopRequireDefault(_generateHttpResponse);

var _register = require('../business/register');

var _register2 = _interopRequireDefault(_register);

var _registerUser = require('./specification/register-user');

var _registerUser2 = _interopRequireDefault(_registerUser);

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

var _agentManagement = require('./agentManagement');

var _agentManagement2 = _interopRequireDefault(_agentManagement);

var _userProfileManagement = require('./userProfileManagement');

var _userProfileManagement2 = _interopRequireDefault(_userProfileManagement);

var _UUIDManagement = require('./UUIDManagement');

var _UUIDManagement2 = _interopRequireDefault(_UUIDManagement);

var _check = require('express-validator/check');

var _filter = require('express-validator/filter');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//validation libraries
var registerManagement = _express2.default.Router();
var reg = new _register2.default();

registerManagement.use('/agent', _agentManagement2.default);
registerManagement.use('/userProfile', _userProfileManagement2.default);
registerManagement.use('/userManagement', _UUIDManagement2.default);

function validateUplaodRegisterDataModel(req, res, next) {
    new _registerUser2.default().validate(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.badRequest(res, err);
            return false;
        } else {
            next();
            return true;
        }
    });
}

//get all users from database
registerManagement.get('', function (req, res) {
    reg.list('', function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else {
            _generateHttpResponse2.default.ok(res, value);
        }
    });
});

//get user based on email-id
registerManagement.get('/:email', function (req, res) {
    if (_validator2.default.isEmail(req.params.email)) {
        req.params.email = _validator2.default.normalizeEmail(req.params.email);
        reg.list(req.params.email, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.length > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, "Email ID not found");
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, "Invalid query parameter");
    }
});

//add user to database

registerManagement.post('', validateUplaodRegisterDataModel, function (req, res) {
    reg.add(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else if (value) {
            _generateHttpResponse2.default.created(res, value);
        } else {
            _generateHttpResponse2.default.badRequest(res, 'User already exist');
        }
    });
});

//update user login info

registerManagement.put('/:email', function (req, res) {
    if (req.params.email) {
        reg.update(req.params.email, req.body, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'Email id is invalid or user not registered');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid email id');
    }
});

//delete user
registerManagement.delete('/:email', function (req, res) {
    if (req.params.email) {
        reg.remove(req.params.email, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'EmailId not found');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid email id');
    }
});
exports.default = registerManagement;