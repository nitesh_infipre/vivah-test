"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _generateHttpResponse = require('../utilities/generate-http-response');

var _generateHttpResponse2 = _interopRequireDefault(_generateHttpResponse);

var _userid = require('../business/userid');

var _userid2 = _interopRequireDefault(_userid);

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//import UserDetailModel from './specification/user-details';
// var http = require('http');


var userManagement = _express2.default.Router();
var userId = new _userid2.default();

userManagement.post('', function (req, res) {
    var auth = req.headers['authorization'];
    if (auth) {

        var tmp = auth.split(' ');
        var buf = new Buffer(tmp[1], 'base64');
        var plain_auth = buf.toString();

        // console.log("Decoded Authorization ", plain_auth);

        var creds = plain_auth.split(':');
        var username = creds[0];
        var password = creds[1];
        userId.check(username, password, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.unauthorized(res, err);
            } else if (value && value.length > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {}
        });
    } else {
        _generateHttpResponse2.default.notFound(res, 'EmailId not found');
    }
});

exports.default = userManagement;