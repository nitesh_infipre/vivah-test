"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _generateHttpResponse = require('../utilities/generate-http-response');

var _generateHttpResponse2 = _interopRequireDefault(_generateHttpResponse);

var _educationInfo = require('../business/educationInfo');

var _educationInfo2 = _interopRequireDefault(_educationInfo);

var _educationInfoDetails = require('./specification/educationInfo-details');

var _educationInfoDetails2 = _interopRequireDefault(_educationInfoDetails);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var educationInfoManagement = _express2.default.Router();
var education = new _educationInfo2.default();

function validateEducationInfoDataModel(req, res, next) {
    new _educationInfoDetails2.default().validate(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.badRequest(res, err);
            return false;
        } else {
            next();
            return true;
        }
    });
}

//get all education info from database
educationInfoManagement.get('', function (req, res) {
    education.list('', function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else {
            _generateHttpResponse2.default.ok(res, value);
        }
    });
});

educationInfoManagement.get('/:uuid', function (req, res) {
    if (req.params.uuid) {
        education.list(req.params.uuid, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.length > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'UUID not found');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid uuid');
    }
});
//add education details to database
educationInfoManagement.post('', validateEducationInfoDataModel, function (req, res) {
    education.add(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else if (value) {
            _generateHttpResponse2.default.created(res, value);
        } else {
            _generateHttpResponse2.default.badRequest(res, err);
        }
    });
});

//update education details 
educationInfoManagement.put('/:uuid', function (req, res) {
    if (req.params.uuid) {
        education.update(req.params.uuid, req.body, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'uuid is invalid or user not registered');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid uuid');
    }
});

//delete education information
educationInfoManagement.delete('/:uuid', function (req, res) {
    if (req.params.uuid) {
        education.remove(req.params.uuid, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'uuid not found');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid uuid');
    }
});
exports.default = educationInfoManagement;