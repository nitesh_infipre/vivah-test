'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UserPreferenceModel = function () {
    function UserPreferenceModel() {
        _classCallCheck(this, UserPreferenceModel);

        this.model = _joi2.default.object().keys({
            AgeFrom: _joi2.default.number(),
            AgeTo: _joi2.default.number(),
            ReligionId: _joi2.default.number(),
            MinHeight: _joi2.default.number(),
            MaxHeight: _joi2.default.number(),
            MinWeight: _joi2.default.number(),
            MaxWeight: _joi2.default.number(),
            Occupation: _joi2.default.number(),
            Education: _joi2.default.string(),
            Country: _joi2.default.number(),
            PassportType: _joi2.default.string(),
            SkinTone: _joi2.default.number(),
            Built: _joi2.default.string(),
            Drink: _joi2.default.string(),
            Smoke: _joi2.default.string(),
            Diet: _joi2.default.string(),
            LifeStyle: _joi2.default.string(),
            MaritalStatusId: _joi2.default.number(),
            LookingFor: _joi2.default.string(),
            LanguageId: _joi2.default.number(),
            FamilyStatusId: _joi2.default.number()

        });
    }

    _createClass(UserPreferenceModel, [{
        key: 'validate',
        value: function validate(data, callback) {
            _joi2.default.validate(data, this.model, {
                stripUnknown: true
            }, function (err, value) {
                callback(err, value);
            });
        }
    }]);

    return UserPreferenceModel;
}();

exports.default = UserPreferenceModel;