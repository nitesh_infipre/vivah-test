'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var userProfileModel = function () {
    function userProfileModel() {
        _classCallCheck(this, userProfileModel);

        this.model = _joi2.default.object().keys({
            DateOfBirth: _joi2.default.date(),
            Gender: _joi2.default.string(),
            MobileNumber: _joi2.default.string(),
            DoDrink: _joi2.default.string(),
            DoSmoke: _joi2.default.string(),
            AboutYourself: _joi2.default.string(),
            ReligionId: _joi2.default.number(),
            CasteId: _joi2.default.number(),
            GotraId: _joi2.default.number(),
            Height: _joi2.default.number(),
            Weight: _joi2.default.number(),
            SkinToneId: _joi2.default.number(),
            HairColor: _joi2.default.string(),
            EyeColor: _joi2.default.string(),
            Kuldev: _joi2.default.string(),
            IdentificationMark: _joi2.default.string(),
            CurrentLocation: _joi2.default.string(),
            CountryId: _joi2.default.number(),
            CountryBornIn: _joi2.default.string(),
            WillingToRelocate: _joi2.default.string(),
            CultureId: _joi2.default.number(),
            StateId: _joi2.default.number(),
            CityId: _joi2.default.number(),
            MaritalStatusId: _joi2.default.number(),
            MedicalHistory: _joi2.default.string(),
            FamilyStatusId: _joi2.default.number(),
            Hobbies: _joi2.default.string(),
            Age: _joi2.default.number(),
            PassportType: _joi2.default.string(),
            Built: _joi2.default.string(),
            Diet: _joi2.default.string(),
            Lifestyle: _joi2.default.string(),
            LookingFor: _joi2.default.string()

        });
    }

    _createClass(userProfileModel, [{
        key: 'validate',
        value: function validate(data, callback) {
            _joi2.default.validate(data, this.model, {
                stripUnknown: true
            }, function (err, value) {
                callback(err, value);
            });
        }
    }]);

    return userProfileModel;
}();

exports.default = userProfileModel;