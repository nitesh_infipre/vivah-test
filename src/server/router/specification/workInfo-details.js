'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var WorkInfoDetailsModel = function () {
    function WorkInfoDetailsModel() {
        _classCallCheck(this, WorkInfoDetailsModel);

        this.model = _joi2.default.object().keys({
            CompanyOrBusiness: _joi2.default.string(),
            JoiningDate: _joi2.default.date(),
            ResignationDate: _joi2.default.date(),
            Designation: _joi2.default.string(),
            WorkTypeId: _joi2.default.number(),
            Description: _joi2.default.string(),
            AnnualIncome: _joi2.default.number(),
            WorkAddId: _joi2.default.string()
        });
    }

    _createClass(WorkInfoDetailsModel, [{
        key: 'validate',
        value: function validate(data, callback) {
            _joi2.default.validate(data, this.model, {
                stripUnknown: true
            }, function (err, value) {
                callback(err, value);
            });
        }
    }]);

    return WorkInfoDetailsModel;
}();

exports.default = WorkInfoDetailsModel;