"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _swaggerJsdoc = require('swagger-jsdoc');

var _swaggerJsdoc2 = _interopRequireDefault(_swaggerJsdoc);

var _swagger = require('./swagger');

var _swagger2 = _interopRequireDefault(_swagger);

var _registerManagement = require('./registerManagement');

var _registerManagement2 = _interopRequireDefault(_registerManagement);

var _metadataManagement = require('./metadataManagement');

var _metadataManagement2 = _interopRequireDefault(_metadataManagement);

var _partnerPreferenceManagement = require('./partnerPreferenceManagement');

var _partnerPreferenceManagement2 = _interopRequireDefault(_partnerPreferenceManagement);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.use('/user', _registerManagement2.default);

router.use('/preference', _partnerPreferenceManagement2.default);

router.use('/data', _metadataManagement2.default);

router.get('/docs.json', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send((0, _swaggerJsdoc2.default)(_swagger2.default));
});

exports.default = router;