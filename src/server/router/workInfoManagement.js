"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _generateHttpResponse = require('../utilities/generate-http-response');

var _generateHttpResponse2 = _interopRequireDefault(_generateHttpResponse);

var _workInfo = require('../business/workInfo');

var _workInfo2 = _interopRequireDefault(_workInfo);

var _workInfoDetails = require('./specification/workInfo-details');

var _workInfoDetails2 = _interopRequireDefault(_workInfoDetails);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var workInfoManagement = _express2.default.Router();
var work = new _workInfo2.default();

function validateWorkInfoDataModel(req, res, next) {
    new _workInfoDetails2.default().validate(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.badRequest(res, err);
            return false;
        } else {
            next();
            return true;
        }
    });
}

//add work info to database
workInfoManagement.post('', validateWorkInfoDataModel, function (req, res) {
    work.add(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else if (value) {
            _generateHttpResponse2.default.created(res, value);
        } else {
            _generateHttpResponse2.default.badRequest(res, err);
        }
    });
});

//get all work info from database
workInfoManagement.get('', function (req, res) {
    work.list('', function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else {
            _generateHttpResponse2.default.ok(res, value);
        }
    });
});

//update work information
workInfoManagement.put('/:uuid', function (req, res) {
    if (req.params.uuid) {
        work.update(req.params.uuid, req.body, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'uuid is invalid or user not registered');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid uuid');
    }
});

//delete work information
workInfoManagement.delete('/:uuid', function (req, res) {
    if (req.params.uuid) {
        work.remove(req.params.uuid, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'uuid not found');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid uuid');
    }
});
exports.default = workInfoManagement;