"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _generateHttpResponse = require('../utilities/generate-http-response');

var _generateHttpResponse2 = _interopRequireDefault(_generateHttpResponse);

var _partnerPreference = require('../business/partnerPreference');

var _partnerPreference2 = _interopRequireDefault(_partnerPreference);

var _userPreference = require('./specification/user-preference');

var _userPreference2 = _interopRequireDefault(_userPreference);

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PrefernceManagement = _express2.default.Router();
var preferences = new _partnerPreference2.default();

function validateUplaodRegisterDataModel(req, res, next) {
    new _userPreference2.default().validate(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.badRequest(res, err);
            return false;
        } else {
            next();
            return true;
        }
    });
}

//get all users from database
/* searchManagement.get('', (req, res) => {
    ser.list('', (err, value) => {
        if (err) {
            HttpReponseUtil.internalServerError(res, err);
        } else {
            HttpReponseUtil.ok(res, value);
        }
    });
}); */

//get user based on email-id
PrefernceManagement.get('/:uuid', function (req, res) {

    preferences.list(req.params.uuid, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else if (value && value.length > 0) {
            _generateHttpResponse2.default.ok(res, value);
        } else {
            _generateHttpResponse2.default.notFound(res, 'uuid not found');
        }
    });
});

PrefernceManagement.get('', function (req, res) {
    preferences.list('', function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else {
            _generateHttpResponse2.default.ok(res, value);
        }
    });
});

PrefernceManagement.post('', validateUplaodRegisterDataModel, function (req, res) {
    preferences.add(req.body, function (err, value) {
        if (err) {
            _generateHttpResponse2.default.internalServerError(res, err);
        } else if (value) {
            _generateHttpResponse2.default.created(res, value);
        } else {
            _generateHttpResponse2.default.badRequest(res, 'User already exist');
        }
    });
});

//update preference
PrefernceManagement.put('/:uuid', function (req, res) {
    if (req.params.uuid) {
        preferences.update(req.params.uuid, req.body, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, err);
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid UUID');
    }
});

//delete work information
PrefernceManagement.delete('/:uuid', function (req, res) {
    if (req.params.uuid) {
        preferences.remove(req.params.uuid, function (err, value) {
            if (err) {
                _generateHttpResponse2.default.internalServerError(res, err);
            } else if (value && value.affectedRows > 0) {
                _generateHttpResponse2.default.ok(res, value);
            } else {
                _generateHttpResponse2.default.notFound(res, 'uuid not found');
            }
        });
    } else {
        _generateHttpResponse2.default.badRequest(res, 'Enter valid uuid');
    }
});

exports.default = PrefernceManagement;