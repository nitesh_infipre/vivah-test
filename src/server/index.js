"use strict";

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _router = require('./router');

var _router2 = _interopRequireDefault(_router);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var cors = require('cors');

var app = (0, _express2.default)();



app.use(cors());

app.use(_bodyParser2.default.urlencoded({
    extended: true
}));
app.use(_bodyParser2.default.json());

app.use('/api', _router2.default);

// Set Static Folder
app.use(_express2.default.static(_path2.default.join(__dirname, 'public')));

app.use(_express2.default.static(__dirname + '/uploads'));

// Index Route
app.get('/', function (req, res) {
    res.send('Invalid Endpoint');
});

app.get('*', function (req, res) {
    res.sendFile(_path2.default.join(__dirname, 'public/index.html'));
});

var PORT = 3002;
// const ip ='192.168.1.171';
app.listen(PORT, function () {
    console.log('app listening on port! ' + PORT);
});