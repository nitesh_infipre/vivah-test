'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// datalayer

var Register = function () {
    function Register(db) {
        _classCallCheck(this, Register);

        this.db = db;
    }

    //add user to database.


    _createClass(Register, [{
        key: 'add',
        value: function add(user, callback) {
            var _this = this;

            var query = 'INSERT INTO login(uuid,first_name,last_name,email,password,userOrAgent,status) \n         VALUES(\'' + user.uuid + '\',\'' + user.firstname + '\',\'' + user.lastname + '\',\n         \'' + user.email + '\',\'' + user.password + '\',\'' + user.agent + '\',1);';
            this.db.executeQuery(query, user, function (err, results, fields) {

                if (err) {
                    callback(err, results);
                } else {
                    //add record to user table
                    var agentId = '45171487-bf9b-11e7-9321-f828194d1b12';

                    //plan id logic is not implemented yet
                    var sql1 = 'INSERT INTO user (uuid,user_id,agent_uuid,plan_id) VALUES (\n                            uuid(),\'' + user.uuid + '\',\'' + agentId + '\',\'\');';

                    var sql2 = 'INSERT INTO personal_info(uuid,user_uuid) VALUES (uuid(),\'' + user.uuid + '\')';

                    //add record to the personl info table
                    _this.db.executeQuery(sql2, function (err, results, fields) {
                        _this.db.executeQuery(sql1, function (err, results, fields) {
                            results.uuid = user.uuid; //send user uuid in response
                            callback(err, results);
                        });
                    });
                }
            });
        }

        //get all list of users

    }, {
        key: 'selectAll',
        value: function selectAll(callback) {
            var query = 'SELECT * FROM login;';
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }

        //get list of users based on email id 

    }, {
        key: 'selectByEmail',
        value: function selectByEmail(email, callback) {
            var query = 'SELECT * FROM login where email = \'' + email + '\';';
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }

        //update user details

    }, {
        key: 'update',
        value: function update(email, user, callback) {
            var _this2 = this;

            this.selectByEmail(email, function (err, val) {
                var query = 'update login SET first_name = ?, last_name = ?, password = ? where email = "' + email + '"';
                _this2.db.executeQuery(query, [user.firstname || val[0].first_name, user.lastname || val[0].last_name, user.UserPassword || val[0].password], function (err, results, fields) {
                    callback(err, results);
                });
            });
        }

        //remove user

    }, {
        key: 'remove',
        value: function remove(email, callback) {
            var query = 'update login SET status = 0 where email = \'' + email + '\';';
            this.db.executeQuery(query, null, function (err, results, fields) {
                callback(err, results);
            });
        }
    }]);

    return Register;
}();

exports.default = Register;