'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// datalayer

var Agent = function () {
    function Agent(db) {
        _classCallCheck(this, Agent);

        this.db = db;
    }

    //add agent details to database.


    _createClass(Agent, [{
        key: 'add',
        value: function add(agent, callback) {
            var query = 'INSERT INTO agent(uuid,user_uuid,agent_type,creation_date,reneval_date,balance_remaining,plan_id,status)  \n        VALUES(uuid(),\'' + agent.uuid + '\',\'' + agent.AgentType + '\',now(),DATE_ADD(now(), INTERVAL 12 MONTH),\'' + agent.Balance + '\',\'' + agent.PlanId + '\',1);';
            this.db.executeQuery(query, agent, function (err, results, fields) {
                callback(err, results);
                // console.log(query);
            });
        }

        //update agent details

    }, {
        key: 'update',
        value: function update(uuid, agent, callback) {

            var query = 'update agent SET agent_type = ? where uuid = "' + uuid + '"';
            this.db.executeQuery(query, [agent.AgentType || ''], function (err, results, fields) {
                console.log(results);
                callback(err, results);
            });
        }

        //get all list of agents

    }, {
        key: 'selectAll',
        value: function selectAll(callback) {
            var query = 'SELECT * FROM agent;';
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }

        //remove agent

    }, {
        key: 'remove',
        value: function remove(uuid, callback) {
            var query = 'update agent SET status = 0 where uuid = \'' + uuid + '\';';
            this.db.executeQuery(query, null, function (err, results, fields) {
                callback(err, results);
            });
        }
    }]);

    return Agent;
}();

exports.default = Agent;