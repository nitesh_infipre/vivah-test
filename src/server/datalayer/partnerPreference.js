"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// datalayer

var Search = function () {
    function Search(db) {
        _classCallCheck(this, Search);

        this.db = db;
    }

    //get all list of users


    _createClass(Search, [{
        key: "selectAll",
        value: function selectAll(callback) {
            var query = "SELECT * FROM partner_preference;";
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }, {
        key: "selectByUUID",
        value: function selectByUUID(uuid, callback) {

            var query = "SELECT * FROM partner_preference where uuid = '" + uuid + "';";
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }, {
        key: "selectUUID",
        value: function selectUUID(uuid, callback) {
            var query = "SELECT * FROM partner_preference where uuid = '" + uuid + "';";

            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }

        //adding preference

    }, {
        key: "add",
        value: function add(preference, callback) {
            //console.log(preference);
            var query = "INSERT INTO partner_preference(uuid, personal_info_uuid, age_from, age_to, \n            religion_id, height_min, height_max, weight_min, weight_max, occupation, education,\n         country_id, passport_visa_type, skin_tone_id, built, drink, smoke, diet, lifestyle,\n          marital_status_id, looking_for, language_id, family_status_id,status)\n            VALUES(uuid(),'" + preference.UserUUID + "','" + preference.AgeFrom + "','" + preference.AgeTo + "','" + preference.ReligionId + "',\n            '" + preference.MinHeight + "','" + preference.MaxHeight + "','" + preference.MinWeight + "','" + preference.MaxWeight + "',\n            '" + preference.Occupation + "','" + preference.Education + "','" + preference.Country + "','" + preference.PassportType + "',\n            '" + preference.SkinTone + "','" + preference.Built + "','" + preference.Drink + "','" + preference.Smoke + "',\n            '" + preference.Diet + "','" + preference.LifeStyle + "','" + preference.MaritalStatusId + "',\n            '" + preference.LookingFor + "','" + preference.LanguageId + "','" + preference.FamilyStatusId + "',1);";
            console.log(query);
            this.db.executeQuery(query, preference, function (err, results, fields) {

                callback(err, results);
            });
        }

        //update profile

    }, {
        key: "update",
        value: function update(uuid, preference, callback) {
            var _this = this;

            this.selectUUID(uuid, function (err, value) {
                console.log(preference);
                var query = "update partner_preference SET age_from = ?, age_to = ?,religion_id = ?, height_min = ?,\n              height_max = ?,  weight_min = ?,weight_max = ?, passport_visa_type = ?,built=?,drink = ?, smoke= ?,\n               diet=?, lifestyle = ?,looking_for = ? ,language_id = ?,family_status_id = ?, occupation = ?,\n               education = ?, country_id = ?, skin_tone_id = ?,marital_status_id = ? where uuid = \"" + uuid + "\"";
                _this.db.executeQuery(query, [preference.AgeFrom || value[0].age_from, preference.AgeTo || value[0].age_to, preference.ReligionId || value[0].religion_id, preference.MinHeight || value[0].height_min, preference.MaxHeight || value[0].height_max, preference.MinWeight || value[0].weight_min, preference.MaxWeight || value[0].weight_max, preference.PassportType || value[0].passport_visa_type, preference.Built || value[0].built, preference.Drink || value[0].drink, preference.Smoke || value[0].smoke, preference.Diet || value[0].diet, preference.LifeStyle || value[0].lifestyle, preference.LookingFor || value[0].looking_for, preference.LanguageId || value[0].language_id, preference.FamilyStatusId || value[0].family_status_id, preference.ocupation || value[0].occupation, preference.Education || value[0].education, preference.Country || value[0].country_id, preference.SkinTone || value[0].skin_tone_id, preference.MaritalStatusId || value[0].marital_status_id], function (err, results, fields) {
                    callback(err, results);
                });
            });
        }
    }, {
        key: "remove",
        value: function remove(uuid, callback) {
            var query = "update partner_preference SET status = 0 where uuid = '" + uuid + "';";
            this.db.executeQuery(query, null, function (err, results, fields) {
                callback(err, results);
            });
        }
    }]);

    return Search;
}();

exports.default = Search;