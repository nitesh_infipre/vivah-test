"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// datalayer

var Search = function () {
    function Search(db) {
        _classCallCheck(this, Search);

        this.db = db;
    }

    //get all list of users


    _createClass(Search, [{
        key: "selectAll",
        value: function selectAll(callback) {
            var query = "SELECT * FROM user;";
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }, {
        key: "selectByEmail",
        value: function selectByEmail(email, callback) {
            var query = "SELECT * FROM user where email = '" + email + "';";

            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }, {
        key: "selectByDetails",
        value: function selectByDetails(name, email, callback) {
            var query = "SELECT Name, State FROM user where email = '" + email + "' and name = '" + name + "';";
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }, {
        key: "selectByName",
        value: function selectByName(name, callback) {
            var query = "SELECT Name FROM user where name = '" + name + "';";
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }]);

    return Search;
}();

exports.default = Search;