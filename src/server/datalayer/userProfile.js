'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// datalayer

var UserProfile = function () {
    function UserProfile(db) {
        _classCallCheck(this, UserProfile);

        this.db = db;
    }

    //add profile info to database.


    _createClass(UserProfile, [{
        key: 'add',
        value: function add(profile, callback) {
            var query = 'INSERT INTO personal_info(uuid,user_uuid,date_of_birth,gender,mobile_number,age,do_drink,\n            do_smoke,about_yourself,religion_id,caste_id,gotra_id,height,weight,skin_tone_id,hair_color,\n            eye_color,kuldev,identification_mark,current_location,country_id,country_born_in,willing_to_relocate,\n            culture_id,state_id,city_id,marital_status_id,medical_history,family_status_id,hobbies)\n            VALUES(uuid(),\'' + profile.UserUUID + '\',\'' + profile.DateOfBirth + '\',\'' + profile.Gender + '\',\'' + profile.MobileNumber + '\',\n            \'' + profile.Age + '\',\'' + profile.DoDrink + '\',\'' + profile.DoSmoke + '\',\'' + profile.AboutYourself + '\',\'' + profile.RegionId + '\',\n            \'' + profile.CasteId + '\',\'' + profile.GotraId + '\',\'' + profile.Height + '\',\'' + profile.Weight + '\',\n            \'' + profile.SkinToneId + '\',\'' + profile.HairColor + '\',\'' + profile.EyeColor + '\',\'' + profile.Kuldev + '\',\n            \'' + profile.IdentificationMark + '\',\'' + profile.CurrentLocation + '\',\'' + profile.CountryId + '\',\n            \'' + profile.CountryBornIn + '\',\'' + profile.WillingToRelocate + '\',\'' + profile.CultureId + '\',\n            \'' + profile.StateId + '\',\'' + profile.CityId + '\',\'' + profile.MaritalStatusId + '\',\n            \'' + profile.MedicalHistory + '\',\'' + profile.FamilyStatusId + '\',\'' + profile.Hobbies + '\');';
            console.log(query);

            this.db.executeQuery(query, profile, function (err, results, fields) {
                console.log(err);
                callback(err, results);
            });
        }

        //update profile

    }, {
        key: 'update',
        value: function update(uuid, profile, callback) {
            var _this = this;

            this.selectUUID(uuid, function (err, value) {

                if (err) {
                    callback(err, null);
                }

                if (value.length === 0) {
                    callback('uuid invalid', null);
                }

                var query = 'update personal_info SET date_of_birth = ?, gender = ?,mobile_number = ?,age=?, do_drink = ?, do_smoke = ?, \n            about_yourself = ?, religion_id = ?, caste_id = ?, gotra_id = ?, height = ?, weight = ?, skin_tone_id = ?,\n             hair_color = ?, eye_color = ?, kuldev = ?, identification_mark = ?, current_location = ?, country_id = ?,\n              country_born_in = ?, willing_to_relocate = ?, culture_id = ?, state_id = ?, city_id = ?,\n               marital_status_id = ? , medical_history = ?, family_status_id = ?, hobbies = ?,profile_picture = ?\n               where uuid = "' + uuid + '"';
                _this.db.executeQuery(query, [profile.DateOfBirth || value[0].date_of_birth, profile.Gender || value[0].gender, profile.MobileNumber || value[0].mobile_number, profile.Age || value[0].age, profile.DoDrink || value[0].do_drink, profile.DoSmoke || value[0].do_smoke, profile.AboutYourself || value[0].about_yourself, profile.RegionId || value[0].religion_id, profile.CasteId || value[0].caste_id, profile.GotraId || value[0].gotra_id, profile.Height || value[0].height, profile.Weight || value[0].weight, profile.SkinToneId || value[0].skin_tone_id, profile.HairColor || value[0].hair_color, profile.EyeColor || value[0].eye_color, profile.Kuldev || value[0].kuldev, profile.IdentificationMark || value[0].identification_mark, profile.CurrentLocation || value[0].current_location, profile.CountryId || value[0].culture_id, profile.CountryBornIn || value[0].country_born_in, profile.WillingToRelocate || value[0].willing_to_relocate, profile.CultureId || value[0].culture_id, profile.StateId || value[0].state_id, profile.CityId || value[0].city_id, profile.MaritalStatusId || value[0].marital_status_id, profile.MedicalHistory || value[0].medical_history, profile.FamilyStatusId || value[0].family_status_id, profile.Hobbies || value[0].hobbies, profile.profile_picture || value[0].profile_picture], function (err, results, fields) {
                    callback(err, results);
                });
            });
            //console.log(uuid);

        }

        //get details of profile

    }, {
        key: 'selectAll',
        value: function selectAll(callback) {
            var query = 'SELECT * FROM personal_info;';
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }, {
        key: 'selectByUUID',
        value: function selectByUUID(uuid, callback) {

            var query = 'SELECT uuid, user_uuid, date_of_birth, gender, mobile_number,age, do_drink, do_smoke, about_yourself, religion.religion,caste.caste, gotra.gotra, height, weight, skin_tone.value AS \'skin tone\', hair_color, eye_color, kuldev, identification_mark, current_location, country.country_name, country_born_in, willing_to_relocate,culture.culture , state.state_name, city.city_name,marital_status.status AS \'Marital Status\',medical_history,family_status.family_status,hobbies FROM personal_info JOIN religion ON religion.religion_id = personal_info.religion_id JOIN caste ON personal_info.caste_id = caste.caste_id JOIN gotra ON gotra.gotra_id= personal_info.gotra_id JOIN skin_tone ON skin_tone.skin_tone_id = personal_info.skin_tone_id JOIN country ON country.country_id = personal_info.country_id JOIN culture ON culture.culture_id = personal_info.culture_id JOIN state ON state.state_id = personal_info.state_id JOIN city ON city.city_id = personal_info.caste_id JOIN marital_status ON marital_status.marital_status_id = personal_info.marital_status_id  JOIN family_status ON family_status.family_status_id = personal_info.family_status_id WHERE uuid = \'' + uuid + '\';';
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }, {
        key: 'selectUUID',
        value: function selectUUID(uuid, callback) {
            var query = 'SELECT * FROM personal_info where uuid = \'' + uuid + '\';';
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
        //remove user

    }, {
        key: 'remove',
        value: function remove(uuid, callback) {
            var query = 'delete from personal_info where uuid = \'' + uuid + '\';';
            this.db.executeQuery(query, null, function (err, results, fields) {
                callback(err, results);
            });
        }
    }, {
        key: 'getUserStatus',
        value: function getUserStatus(uuid, callback) {
            var query = 'SELECT user_id,is_done_personal_info FROM user WHERE user_id=\'' + uuid + '\';';
            this.db.executeQuery(query, function (err, results, fields) {
                callback(err, results);
            });
        }
    }, {
        key: 'getUserPersonalInfoId',
        value: function getUserPersonalInfoId(uuid, callback) {
            var query = 'SELECT uuid FROM personal_info WHERE user_uuid=\'' + uuid + '\';';
            this.db.executeQuery(query, function (err, results, fields) {
                callback(err, results);
            });
        }
    }]);

    return UserProfile;
}();

exports.default = UserProfile;