"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// datalayer

var EducationInfo = function () {
    function EducationInfo(db) {
        _classCallCheck(this, EducationInfo);

        this.db = db;
    }

    //add education details to database.


    _createClass(EducationInfo, [{
        key: "add",
        value: function add(education, callback) {
            var query = "INSERT INTO educationinfo(uuid,user_info_uuid,examination,specialization,year_of_passing,\n         university,percentage,status) VALUES(uuid(),'" + education.UserInfoUUID + "','" + education.examination + "',\n            '" + education.specialization + "','" + education.yearOfPassing + "','" + education.University + "',\n            '" + education.percentage + "',1);";
            this.db.executeQuery(query, education, function (err, results, fields) {
                callback(err, results);
            });
        }

        //get all list of education details

    }, {
        key: "selectAll",
        value: function selectAll(callback) {
            var query = "SELECT * FROM educationinfo;";
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }, {
        key: "selectByUUID",
        value: function selectByUUID(uuid, callback) {
            var query = "SELECT * FROM educationinfo where uuid = '" + uuid + "';";
            console.log(query);
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }, {
        key: "selectUUID",
        value: function selectUUID(uuid, callback) {
            var query = "SELECT * FROM educationinfo where uuid = '" + uuid + "';";
            console.log(query);
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }

        //update education details

    }, {
        key: "update",
        value: function update(uuid, education, callback) {
            var _this = this;

            this.selectUUID(uuid, function (err, value) {
                var query = "update educationinfo SET examination = ?,specialization = ?,year_of_passing = ?,\n    university = ?,percentage = ? \n            where uuid = \"" + uuid + "\"";
                _this.db.executeQuery(query, [education.examination || value[0].examination, education.specialization || value[0].specialization, education.yearOfPassing || value[0].year_of_passing, education.University || value[0].university, education.percentage || value[0].percentage], function (err, results, fields) {
                    callback(err, results);
                });
            });
        }

        //remove education details

    }, {
        key: "remove",
        value: function remove(uuid, callback) {
            var query = "update educationinfo set status = 0 where uuid = '" + uuid + "';";
            this.db.executeQuery(query, null, function (err, results, fields) {
                callback(err, results);
            });
        }
    }]);

    return EducationInfo;
}();

exports.default = EducationInfo;