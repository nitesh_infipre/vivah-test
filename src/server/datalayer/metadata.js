'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//datalayer

var Metadata = function () {
	function Metadata(db) {
		_classCallCheck(this, Metadata);

		this.db = db;
	}

	_createClass(Metadata, [{
		key: 'states',
		value: function states(callback) {
			var query = 'SELECT * FROM state;';
			this.db.executeQuery(query, function (err, result, fields) {
				callback(err, result);
			});
		}
	}, {
		key: 'city',
		value: function city(state, callback) {
			var query = 'SELECT * FROM city where state_id=' + state + ';';
			this.db.executeQuery(query, function (err, result, fields) {
				callback(err, result);
			});
		}
	}, {
		key: 'religion',
		value: function religion(callback) {
			var query = 'SELECT * FROM religion;';
			this.db.executeQuery(query, function (err, result, fields) {
				callback(err, result);
			});
		}
	}, {
		key: 'culture',
		value: function culture(callback) {
			var query = 'SELECT * FROM culture;';
			this.db.executeQuery(query, function (err, result, fields) {
				callback(err, result);
			});
		}
	}, {
		key: 'maritalStatus',
		value: function maritalStatus(callback) {
			var query = 'SELECT * FROM marital_status;';
			this.db.executeQuery(query, function (err, result, fields) {
				callback(err, result);
			});
		}
	}, {
		key: 'worktype',
		value: function worktype(callback) {
			var query = 'SELECT * FROM worktype;';
			this.db.executeQuery(query, function (err, result, fields) {
				callback(err, result);
			});
		}
	}, {
		key: 'all',
		value: function all(callback) {
			var _this = this;

			var data = {};
			var queries = {
				'state': 'SELECT * FROM state;',
				'city': 'SELECT * FROM city',
				'religion': 'SELECT * FROM religion'
				// 'culture': 'SELECT * FROM culture',
				// 'marital_status': 'SELECT * FROM marital_status;',
				// 'worktype': 'SELECT * FROM worktype;',
				// 'skintone': 'SELECT * FROM vivah.skin_tone;'
			};

			var arrayLength = Object.keys(queries).length;
			var index = 0;

			var _loop = function _loop(key) {
				if (queries.hasOwnProperty(key)) {
					_this.db.executeQuery(queries[key], function (err, result, fields) {
						data[key] = result;
						console.log(index);
						index++;
						if (index === arrayLength) {
							console.log("callbak");
							callback(err, data, fields);
						}
					});
				}
			};

			for (var key in queries) {
				_loop(key);
			}
		}
	}]);

	return Metadata;
}();

exports.default = Metadata;