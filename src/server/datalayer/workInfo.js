"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// datalayer

var WorkInfo = function () {
    function WorkInfo(db) {
        _classCallCheck(this, WorkInfo);

        this.db = db;
    }

    //add work information to database.


    _createClass(WorkInfo, [{
        key: "add",
        value: function add(work, callback) {
            var query = "INSERT INTO workinfo(uuid,user_info_uuid,company_business,joining_date,\n            resignation_date,designation,work_type_id,description,annual_income,work_address_id,status)\n              VALUES(uuid(),'" + work.PersonalInfoUUID + "','" + work.CompanyOrBusiness + "','" + work.JoiningDate + "','" + work.ResignationDate + "',\n              '" + work.Designation + "','" + work.WorkTypeId + "','" + work.Description + "',\n              '" + work.AnnualIncome + "','" + work.WorkAddId + "',1);";
            this.db.executeQuery(query, work, function (err, results, fields) {
                callback(err, results);
            });
        }

        //get details of work info

    }, {
        key: "selectAll",
        value: function selectAll(callback) {
            var query = "SELECT * FROM workinfo;";
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }, {
        key: "selectByUUID",
        value: function selectByUUID(uuid, callback) {
            var query = "SELECT company_business, joining_date,resignation_date,designation,worktype.work_type_name,description,annual_income,workaddress.street_address,workaddress.landmark, state.state_name, country.country_name, city.city_name, workaddress.pincode FROM workinfo JOIN worktype ON worktype.work_type_id = workinfo.work_type_id  JOIN workaddress ON workaddress.work_address_id = workinfo.work_address_id JOIN city ON city.city_id = workaddress.city_id JOIN state ON state.state_id = workaddress.state_id JOIN country ON country.country_id = workaddress.country_id WHERE uuid = '" + uuid + "';";
            console.log(query);
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }
    }, {
        key: "selectUUID",
        value: function selectUUID(uuid, callback) {
            var query = "SELECT * FROM workinfo where uuid = '" + uuid + "';";
            console.log(query);
            this.db.executeQuery(query, function (err, result, fields) {
                callback(err, result);
            });
        }

        //update work details

    }, {
        key: "update",
        value: function update(uuid, work, callback) {
            var _this = this;

            this.selectUUID(uuid, function (err, value) {
                var query = "update workinfo SET company_business = ?,joining_date = ?,resignation_date = ?,\n    designation = ?,work_type_id = ?,description = ?,annual_income = ?,work_address_id = ? \n    where uuid = \"" + uuid + "\"";
                _this.db.executeQuery(query, [work.CompanyOrBusiness || value[0].company_business, work.JoiningDate || value[0].joining_date, work.ResignationDate || value[0].resignation_date, work.Designation || value[0].designation, work.WorkTypeId || value[0].work_type_id, work.Description || value[0].description, work.AnnualIncome || value[0].annual_income, work.WorkAddId || value[0].work_address_id], function (err, results, fields) {
                    callback(err, results);
                });
            });
        }

        //remove work details

    }, {
        key: "remove",
        value: function remove(uuid, callback) {
            var query = "Update workinfo set status = 0 where uuid = '" + uuid + "';";
            this.db.executeQuery(query, null, function (err, results, fields) {
                callback(err, results);
            });
        }
    }]);

    return WorkInfo;
}();

exports.default = WorkInfo;