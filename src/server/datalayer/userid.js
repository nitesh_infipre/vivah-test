'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); // datalayer


var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UserId = function () {
    function UserId(db) {
        _classCallCheck(this, UserId);

        this.db = db;
    }

    _createClass(UserId, [{
        key: 'check',
        value: function check(username, password, callback) {
            var _this = this;

            var query1 = 'select password from login where email = \'' + username + '\';';

            this.db.executeQuery(query1, function (err, result, value) {
                // console.log('hellow');
                if (result.length == 0) {
                    err = {
                        'msg': 'username doesnot exists'
                    };
                    callback(err, result);
                } else {

                    var query = 'SELECT uuid FROM login where email = \'' + username + '\' and password = \'' + result[0].password + '\';';
                    _bcrypt2.default.compare(password, result[0].password, function (err, res) {
                        if (res) {
                            _this.db.executeQuery(query, function (err, result, fields) {
                                callback(err, result);
                            });
                        } else {
                            err = {
                                'msg': 'invalid password'
                            };
                            callback(err, result);
                        }
                    });
                }
            });
        }
    }]);

    return UserId;
}();

exports.default = UserId;