'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mysql = require('mysql');

var _mysql2 = _interopRequireDefault(_mysql);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MySqlDB = function () {
    function MySqlDB() {
        var _this = this;

        _classCallCheck(this, MySqlDB);

        this.activeConnections = 0;
        this.pool = _mysql2.default.createPool({
            connectionLimit: 10,
            host: 'localhost',
            user: 'root',
            password: 'root',
            database: 'vivah',
            debug: false
        });

        this.pool.on('acquire', function () {
            _this.activeConnections += 1;
        });

        this.pool.on('release', function () {
            _this.activeConnections -= 1;
        });
    }

    _createClass(MySqlDB, [{
        key: 'executeQuery',
        value: function executeQuery(sql, obj, callback) {
            try {
                this.pool.getConnection(function (err, connection) {
                    if (err) {
                        //  console.log(err);
                        callback(err, null, null);
                    }

                    connection.query(sql, obj, function (error, results, fields) {
                        if (error) {
                            //  console.log(error, sql);
                        }
                        connection.release();
                        callback(err, results, fields);
                    });
                });
            } catch (ex) {
                callback(ex, null, null);
            }
        }
    }, {
        key: 'getActiveConnections',
        value: function getActiveConnections() {
            return this.activeConnections;
        }
    }, {
        key: 'dbExit',
        value: function dbExit() {
            this.pool.end();
        }
    }]);

    return MySqlDB;
}();

var db = new MySqlDB();
exports.default = db;