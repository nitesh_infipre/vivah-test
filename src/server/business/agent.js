'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); //bussiness layer

var _agent = require('../datalayer/agent');

var _agent2 = _interopRequireDefault(_agent);

var _mysql = require('../datalayer/mysql');

var _mysql2 = _interopRequireDefault(_mysql);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Agent = function () {
    function Agent() {
        _classCallCheck(this, Agent);

        this.agentDB = new _agent2.default(_mysql2.default);
    }

    //add agent to databased,


    _createClass(Agent, [{
        key: 'add',
        value: function add(agent, callback) {
            this.agentDB.add(agent, callback);
        }

        //update agent information

    }, {
        key: 'update',
        value: function update(email, agent, callback) {
            this.agentDB.update(email, agent, callback);
        }
        //get users from database

    }, {
        key: 'list',
        value: function list(agent, callback) {
            this.agentDB.selectAll(callback);
        }

        //remove user from database

    }, {
        key: 'remove',
        value: function remove(uuid, callback) {
            this.agentDB.remove(uuid, callback);
        }
    }]);

    return Agent;
}();

exports.default = Agent;