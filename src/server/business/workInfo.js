'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); //bussiness layer

var _workInfo = require('../datalayer/workInfo');

var _workInfo2 = _interopRequireDefault(_workInfo);

var _mysql = require('../datalayer/mysql');

var _mysql2 = _interopRequireDefault(_mysql);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var WorkInfo = function () {
    function WorkInfo() {
        _classCallCheck(this, WorkInfo);

        this.workinfoDB = new _workInfo2.default(_mysql2.default);
    }

    //add work information to databased,


    _createClass(WorkInfo, [{
        key: 'add',
        value: function add(work, callback) {
            this.workinfoDB.add(work, callback);
        }

        //get user work info from database

    }, {
        key: 'list',
        value: function list(uuid, callback) {
            if (uuid) {
                // console.log(uuid);
                this.workinfoDB.selectByUUID(uuid, callback);
            } else {
                this.workinfoDB.selectAll(callback);
            }
        }
        //update work information

    }, {
        key: 'update',
        value: function update(uuid, work, callback) {
            this.workinfoDB.update(uuid, work, callback);
        }

        //remove work info

    }, {
        key: 'remove',
        value: function remove(uuid, callback) {
            this.workinfoDB.remove(uuid, callback);
        }
    }]);

    return WorkInfo;
}();

exports.default = WorkInfo;