'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); //bussiness layer

var _partnerPreference = require('../datalayer/partnerPreference');

var _partnerPreference2 = _interopRequireDefault(_partnerPreference);

var _mysql = require('../datalayer/mysql');

var _mysql2 = _interopRequireDefault(_mysql);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Preference = function () {
    function Preference() {
        _classCallCheck(this, Preference);

        this.preferenceDB = new _partnerPreference2.default(_mysql2.default);
    }

    //add user to databased,


    _createClass(Preference, [{
        key: 'add',
        value: function add(preference, callback) {
            this.preferenceDB.add(preference, callback);
        }

        //get users from database

    }, {
        key: 'list',
        value: function list(uuid, callback) {
            if (uuid) {
                this.preferenceDB.selectByUUID(uuid, callback);
            } else {
                this.preferenceDB.selectAll(callback);
            }
        }

        //update preference information

    }, {
        key: 'update',
        value: function update(uuid, preference, callback) {
            this.preferenceDB.update(uuid, preference, callback);
        }
        //remove user from database

    }, {
        key: 'remove',
        value: function remove(uuid, callback) {
            this.preferenceDB.remove(uuid, callback);
        }
    }]);

    return Preference;
}();

exports.default = Preference;