'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); //bussiness layer

var _educationInfo = require('../datalayer/educationInfo');

var _educationInfo2 = _interopRequireDefault(_educationInfo);

var _mysql = require('../datalayer/mysql');

var _mysql2 = _interopRequireDefault(_mysql);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var EducationInfo = function () {
    function EducationInfo() {
        _classCallCheck(this, EducationInfo);

        this.educationInfoDB = new _educationInfo2.default(_mysql2.default);
    }

    //get education info from database


    _createClass(EducationInfo, [{
        key: 'list',
        value: function list(uuid, callback) {
            if (uuid) {
                // console.log(uuid);
                this.educationInfoDB.selectByUUID(uuid, callback);
            } else {
                this.educationInfoDB.selectAll(callback);
            }
        }
        //add education information to databased,

    }, {
        key: 'add',
        value: function add(education, callback) {
            this.educationInfoDB.add(education, callback);
        }

        //update education information

    }, {
        key: 'update',
        value: function update(uuid, education, callback) {
            this.educationInfoDB.update(uuid, education, callback);
        }

        //remove education info

    }, {
        key: 'remove',
        value: function remove(uuid, callback) {
            this.educationInfoDB.remove(uuid, callback);
        }
    }]);

    return EducationInfo;
}();

exports.default = EducationInfo;