'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _userProfile = require('../datalayer/userProfile');

var _userProfile2 = _interopRequireDefault(_userProfile);

var _mysql = require('../datalayer/mysql');

var _mysql2 = _interopRequireDefault(_mysql);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UserProfile = function () {
    function UserProfile() {
        _classCallCheck(this, UserProfile);

        this.userProfileDB = new _userProfile2.default(_mysql2.default);
    }

    //add user profile to databased,


    _createClass(UserProfile, [{
        key: 'add',
        value: function add(profile, callback) {

            this.userProfileDB.add(profile, callback);
        }

        //update user profile information

    }, {
        key: 'update',
        value: function update(uuid, profile, callback) {
            this.userProfileDB.update(uuid, profile, callback);
        }

        //get user profile from database

    }, {
        key: 'list',
        value: function list(uuid, callback) {
            if (uuid) {

                this.userProfileDB.selectByUUID(uuid, callback);
            } else {
                this.userProfileDB.selectAll(callback);
            }
        }

        //remove user profile from database

    }, {
        key: 'remove',
        value: function remove(uuid, callback) {
            this.userProfileDB.remove(uuid, callback);
        }
    }, {
        key: 'getUserStatus',
        value: function getUserStatus(uuid, callback) {
            this.userProfileDB.getUserStatus(uuid, callback);
        }
    }, {
        key: 'getUserPersonalInfoId',
        value: function getUserPersonalInfoId(uuid, callback) {
            this.userProfileDB.getUserPersonalInfoId(uuid, callback);
        }
    }, {
        key: 'uploadProfile',
        value: function uploadProfile(uuid, filename, callback) {

            this.userProfileDB.update(uuid, { 'profile_picture': filename }, callback);
        }
    }]);

    return UserProfile;
}();

exports.default = UserProfile;