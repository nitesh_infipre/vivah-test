'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mysql = require('../datalayer/mysql');

var _mysql2 = _interopRequireDefault(_mysql);

var _metadata = require('../datalayer/metadata');

var _metadata2 = _interopRequireDefault(_metadata);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Metadata = function () {
	function Metadata() {
		_classCallCheck(this, Metadata);

		this.metaDB = new _metadata2.default(_mysql2.default);
	}

	_createClass(Metadata, [{
		key: 'states',
		value: function states(callback) {
			this.metaDB.states(callback);
		}
	}, {
		key: 'city',
		value: function city(state, callback) {
			this.metaDB.city(state, callback);
		}
	}, {
		key: 'religion',
		value: function religion(callback) {
			this.metaDB.religion(callback);
		}
	}, {
		key: 'culture',
		value: function culture(callback) {
			this.metaDB.culture(callback);
		}
	}, {
		key: 'maritalStatus',
		value: function maritalStatus(callback) {
			this.metaDB.maritalStatus(callback);
		}
	}, {
		key: 'worktype',
		value: function worktype(callback) {
			this.metaDB.maritalStatus(callback);
		}
	}, {
		key: 'all',
		value: function all(callback) {

			this.metaDB.all(callback);
		}
	}]);

	return Metadata;
}();

exports.default = Metadata;