'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); //bussiness layer

var _register = require('../datalayer/register');

var _register2 = _interopRequireDefault(_register);

var _mysql = require('../datalayer/mysql');

var _mysql2 = _interopRequireDefault(_mysql);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _uuid = require('uuid');

var uuid = _interopRequireWildcard(_uuid);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Register = function () {
    function Register() {
        _classCallCheck(this, Register);

        this.registerDB = new _register2.default(_mysql2.default);
    }

    //add user to database,


    _createClass(Register, [{
        key: 'add',
        value: function add(user, callback) {
            var _this = this;

            //get user based on email id and check if it already exists
            this.registerDB.selectByEmail(user.email, function (err, value) {
                if (err) {
                    callback(err);
                }
                //means the email id already exits 
                else if (value && value.length > 0) {
                        callback(err);
                    } else {
                        _bcrypt2.default.hash(user.password, 10, function (err, hash) {
                            if (err) {
                                callback(err);
                            } else {
                                user.password = hash;
                                user.uuid = uuid.v4();

                                _this.registerDB.add(user, callback);
                            }
                        });
                    }
            });
        }

        //get users from database

    }, {
        key: 'list',
        value: function list(email, callback) {
            if (email) {
                this.registerDB.selectByEmail(email, callback);
            } else {
                this.registerDB.selectAll(callback);
            }
        }

        //update user information

    }, {
        key: 'update',
        value: function update(email, user, callback) {
            var _this2 = this;

            _bcrypt2.default.hash(user.UserPassword, 10, function (err, hash) {
                if (err) {
                    callback(err);
                } else {
                    user.UserPassword = hash;
                    // console.log('hash: ' + hash);
                    _this2.registerDB.update(email, user, callback);
                }
            });
        }

        //remove user from database

    }, {
        key: 'remove',
        value: function remove(email, callback) {
            this.registerDB.remove(email, callback);
        }
    }]);

    return Register;
}();

exports.default = Register;