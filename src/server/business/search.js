'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _search = require('../datalayer/search');

var _search2 = _interopRequireDefault(_search);

var _mysql = require('../datalayer/mysql');

var _mysql2 = _interopRequireDefault(_mysql);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Search = function () {
    function Search() {
        _classCallCheck(this, Search);

        this.searchDB = new _search2.default(_mysql2.default);
    }

    _createClass(Search, [{
        key: 'list',
        value: function list(email, callback) {
            if (email) {
                this.searchDB.selectByEmail(email, callback);
            } else {
                this.searchDB.selectAll(callback);
            }
        }
    }, {
        key: 'listName',
        value: function listName(name, callback) {
            if (name) {
                this.searchDB.selectByName(name, callback);
            } else {
                this.searchDB.selectAll(callback);
            }
        }
    }, {
        key: 'listDetails',
        value: function listDetails(name, email, callback) {
            if (name && email) {
                this.searchDB.selectByDetails(name, email, callback);
            } else {
                this.searchDB.selectAll(callback);
            }
        }
    }]);

    return Search;
}();

exports.default = Search;